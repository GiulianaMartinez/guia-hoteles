$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 2000
    });

    var triggeredButton;
    $('#contacto').on('show.bs.modal', function (e){
        console.log('El modal se está mostrando');
        triggeredButton = $(e.relatedTarget);
        $(e.relatedTarget).removeClass('btn-outline-success');
        $(e.relatedTarget).addClass('btn-primary');
        $(e.relatedTarget).prop('disabled', true);
    });

    $('#contacto').on('shown.bs.modal', function (e){
        console.log('El modal se mostró');
    });

    $('#contacto').on('hide.bs.modal', function (e){
        console.log('El modal se oculta');
    });
    
    $('#contacto').on('hidden.bs.modal', function (e){
        console.log('El modal se ocultó');
        triggeredButton.removeClass('btn-primary');
        triggeredButton.addClass('btn-outline-success');
        triggeredButton.prop('disabled', false);
    });
});
